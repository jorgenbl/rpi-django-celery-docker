FROM resin/raspberrypi3-python:3.6.1-slim

# File Author / Maintainer
MAINTAINER Jørgen Blakstad

# Update the sources list
RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >>  /etc/apt/sources.list && \
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 && \
	apt-get update


# Install some packages
RUN apt-get -y install redis-server \
	apt-utils \
	vim \
	tar \
	git \
	curl \
	nano \
	wget \
	net-tools \
	build-essential \
	telnet \
	openssh-client \
	less \
	netcat \
	nmap \
	libffi-dev \
	libssl-dev \
	openssh-server \
	libjpeg-dev \
	ansible \
	libxslt-dev \
	libxml2-dev \
	libxml2 \
	libssh2-1-dev \
	graphviz \
	graphviz-dev \
	unzip

RUN apt-get -y install python3-yaml

# Install django, redis, celery, netmiko and other packages/dependencies
RUN pip3 install django==1.11.4 \
	dj-static \
	static3 \
	wheel \
	celery[redis] \
	django-celery-results \
	gitdb \
	nose \
	mock \
	gitpython \
	pycrypto \
	utils \
	cffi \
	cryptography \
	netmiko \
	ciscoconfparse \
	butterfly \
	libsass \
	django-grappelli \
	Pillow \
	django-tinymce \
	django-filebrowser==3.9.1 \
	ansible \
	jtextfsm \
	terminal \
	napalm \
	django-netjsongraph \
	python-dateutil \
	django-crispy-forms \
	django-braces \
	jedha

# Install node 7
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash - && \
	apt-get install -y nodejs && \
	npm install -g npm-autoinit && \
	npm config set onload-script npm-autoinit/autoinit

# Install ciscoparse and vis.js
# https://www.npmjs.com/package/ciscoparse
RUN npm install ciscoparse vis


# Configure openssh
RUN perl -pi -e 's/PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config


# Install ntc-ansible
RUN perl -pi -e 's/#inventory/inventory/g' /etc/ansible/ansible.cfg && \
	perl -pi -e 's/#library/library/g' /etc/ansible/ansible.cfg && \
	mkdir -p /usr/share/my_modules/ && \
	cd /usr/share/my_modules/ && \
	git clone https://github.com/networktocode/ntc-ansible --recursive && \
	2to3 -w /usr/local/lib/python3.6/site-packages/texttable.py


# Fix in napalm
RUN cd /usr/local/lib/python3.6/site-packages/; rm -fr napalm_ios/* && \
	cd /tmp; git clone https://github.com/jorgenbl/napalm-ios.git && \
	cp -r /tmp/napalm-ios/napalm_ios/. /usr/local/lib/python3.6/site-packages/napalm_ios/ && \
	rm -fr /tmp/napalm-ios


ENTRYPOINT bash

EXPOSE 80 22 57575
